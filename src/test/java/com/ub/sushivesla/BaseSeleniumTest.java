package com.ub.sushivesla;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SeleniumExtension.class)
public class BaseSeleniumTest {

    protected static final String SITE_URL = "https://svw.unitbean.ru";
}
