package com.ub.sushivesla.pages;

import com.ub.sushivesla.BaseSeleniumTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest extends BaseSeleniumTest {

    @DisplayName("Selects Volgograd city and checks selected result")
    @Test
    public void selectAndCheckVolgogradCity(ChromeDriver chromeDriver) {
        HomePage homePage = new HomePage(chromeDriver);
        homePage.navigateToPage(SITE_URL);
        homePage.closeMainBanner();

        homePage.openCityList();
        homePage.selectCity("Волгоград");

        homePage.verifySelectedCity("Волгоград");
    }

    @DisplayName("Selects Sochi city and checks selected result")
    @Test
    public void selectAndCheckSochiCity(ChromeDriver chromeDriver) {
        HomePage homePage = new HomePage(chromeDriver);
        homePage.navigateToPage(SITE_URL);
        homePage.closeMainBanner();

        homePage.openCityList();
        homePage.scrollDownCityList(500);
        homePage.selectCity("Сочи");

        homePage.verifySelectedCity("Сочи");
    }

}
