package com.ub.sushivesla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataDrivenTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataDrivenTestApplication.class, args);
	}

}
