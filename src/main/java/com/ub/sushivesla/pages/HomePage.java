package com.ub.sushivesla.pages;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.ub.sushivesla.Utils.explicitWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElementsLocatedBy;

public class HomePage {

    private final WebDriver driver;
    private final WebDriverWait wait;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    public void navigateToPage(String siteUrl) {
        driver.get(siteUrl);
    }

    private void scrollElementTop(String cssSelector, int value) {
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        eventFiringWebDriver.executeScript("document.querySelector(' " + cssSelector + "').scrollTop=" + value);
    }

    public void closeMainBanner() {
        WebElement openBannerButton = driver.findElement(By.xpath("//*[@id=\"modalBanner\"]/div/div/button"));
        openBannerButton.click();
    }

    public void openCityList() {
        WebElement citySelectorA = driver.findElement(By.xpath("//*[@id=\"navbarFixedTop\"]/div/ul[2]/li[2]/a"));
        citySelectorA.click();

        wait.until(visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"mCSB_1_container\"]")));
    }

    public void scrollDownCityList(int value) {
        scrollElementTop(Constants.CITY_MODAL_CSS_SELECTOR, value);
    }

    public void selectCity(String city) {
        WebElement selectCityLink = driver.findElements(By.xpath("//*[@id=\"mCSB_1_container\"]/div/a"))
                .stream()
                .filter(webElement -> webElement.getText().equalsIgnoreCase(city))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("City with name " + city + " not found"));
        selectCityLink.click();

        explicitWait();
    }

    public void verifySelectedCity(String expectedCityName) {
        WebElement spanSelectedCity = driver.findElement(By.xpath("//*[@id=\"navbarFixedTop\"]/div/ul[2]/li[2]/a/span"));
        Assertions.assertEquals(expectedCityName, spanSelectedCity.getText());
    }

    private interface Constants {
        String CITY_MODAL_CSS_SELECTOR = "#mCSB_1";
    }
}
